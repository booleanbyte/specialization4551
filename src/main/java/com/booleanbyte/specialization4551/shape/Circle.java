package com.booleanbyte.specialization4551.shape;

import java.util.Collection;
import java.util.Map;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import javafx.scene.canvas.GraphicsContext;

public class Circle extends AbstractShape {

	private final Vector2D c;
	private final double h;
	private final double r;

	public Circle(Vector2D c, double radius, double height) {
		this.c = c;
		this.h = height;
		this.r = radius;
	}

	@Override
	public double sdf(Vector2D p) {
		return p.distance(c) - r;
	}
	
	@Override
	public double height(Vector2D p) {
		return h;
	}

	@Override
	public void paint(GraphicsContext g) {
		g.strokeOval(c.getX(), c.getY(), 2*r, 2*r);
	}

	@Override
	public void collectPoints(Map<Vector2D, Double> pointsCollection) {
		pointsCollection.put(c, h);
	}

	@Override
	public void collect2dVerticies(Collection<Vector2D> verticies) {
		verticies.add(c);
	}

	@Override
	public void collect3dVerticies(Collection<Vector3D> verticies) {
		verticies.add(new Vector3D(c.getX(), c.getY(), h));
	}
}
