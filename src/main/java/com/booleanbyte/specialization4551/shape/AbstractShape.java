package com.booleanbyte.specialization4551.shape;

import java.util.Collection;
import java.util.Map;
import java.util.function.Function;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public abstract class AbstractShape {
	
	private Color annotationColor = Color.WHITE;
	
	/**
	 * Calculates the signed distance to the shape from point p.
	 * 
	 * @param p The point to evaluate the shape distance for
	 * @return the signed distance to the shape at point p
	 */
	public abstract double sdf(Vector2D p);

	/**
	 * Calculates the height of a shape at point p.
	 * 
	 * @param p The point to evaluate the shape height for
	 * @return height at point p
	 */
	public abstract double height(Vector2D p);
	
	/**
	 * Adds the weight height from this shape to the sum at point p
	 * 
	 * @param p The point to evaluate the weight for
	 * @param weightedAverage The weight and height sum to be added to
	 * @param distanceWeightFunction
	 */
	public void sumWH(Vector2D p, WeigthedAverage weightedAverage, Function<Double, Double> distanceWeightFunction) {
		weightedAverage.addWeightAndHeight(distanceWeightFunction.apply(sdf(p)), height(p));
	}

	/**
	 * Draw simplified shape in 2D on a JavaFX canvas using GraphicsContext. The
	 * graphics context is transformed by the preview renderer calling this method,
	 * so the shape should be painted using the true coordinates of the shape
	 * without any translation or rotation transforms by this method.
	 * 
	 * @param g
	 */
	public abstract void paint(GraphicsContext g);
	
	/**
	 * Collects all planar points and their shape heights in the provided map. It is
	 * assumed that all duplicated planar points have the same height and they can
	 * therefore treated as equals.
	 * 
	 * @param pointsCollection
	 */
	public abstract void collectPoints(Map<Vector2D, Double> pointsCollection);
	
	public abstract void collect2dVerticies(Collection<Vector2D> verticies);
	
	public abstract void collect3dVerticies(Collection<Vector3D> verticies);
	
	public void setAnnotationColor(Color annotationColor) {
		this.annotationColor = annotationColor;
	}
	
	public Color getAnnotationColor() {
		return annotationColor;
	}
}
