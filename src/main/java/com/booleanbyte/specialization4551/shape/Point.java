package com.booleanbyte.specialization4551.shape;

import java.util.Collection;
import java.util.Map;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.apache.commons.math3.util.MathUtils;

import javafx.scene.canvas.GraphicsContext;

public class Point extends AbstractShape {

	final Vector2D c;
	final double h;
	
	public Point(Vector2D c, double height) {
		this.c = c;
		this.h = height;
	}
	
	public double getX() {
		return c.getX();
	}
	
	public double getY() {
		return c.getY();
	}
	
	@Override
	public double sdf(Vector2D p) {
		return p.distance(c);
	}
	
	@Override
	public double height(Vector2D p) {
		return h;
	}

	@Override
	public void paint(GraphicsContext g) {
		double x = getX();
		double y = getY();
		g.strokeLine(x-5, y-5, x+5, y+5);
		g.strokeLine(x+5, y-5, x-5, y+5);
	}

	@Override
	public void collectPoints(Map<Vector2D, Double> pointsCollection) {
		pointsCollection.put(c, h);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || obj instanceof Point == false) {
			return false;
		}
		
		Point castObj = (Point) obj;
		return c.equals(castObj.c) && h == castObj.h;
	}
	
	@Override
	public int hashCode() {
		return c.hashCode() + MathUtils.hash(h);
	}
	
	@Override
	public void collect2dVerticies(Collection<Vector2D> verticies) {
		verticies.add(c);
	}

	@Override
	public void collect3dVerticies(Collection<Vector3D> verticies) {
		verticies.add(new Vector3D(c.getX(), c.getY(), h));
	}
}
