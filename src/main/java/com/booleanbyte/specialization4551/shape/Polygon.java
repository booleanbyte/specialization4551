package com.booleanbyte.specialization4551.shape;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import javafx.scene.canvas.GraphicsContext;

public class Polygon extends AbstractShape implements ConnectingPointsShape {
	
	final Vector2D[] vertices;
	final double h;
	
	public Polygon(double height, Vector2D... vertices) {
		this.vertices = vertices;
		this.h = height;
	}
	
	public Polygon(double height, List<Vector2D> vertices) {
		this.vertices = vertices.toArray(new Vector2D[0]);
		this.h = height;
	}
	
	@Override
	public boolean areConnected(Vector2D p1, Vector2D p2) {
		int ip2;
		for (ip2 = 0; ip2 < vertices.length; ip2++) {
			if (vertices[ip2].equals(p2)) break;
		}
		if (ip2 >= vertices.length) return false;
		
		int ip1 = ip2-1 >= 0 ? ip2-1 : vertices.length-1;
		int ip3 = ip2+1 < vertices.length ? ip2+1 : 0;
		
		// Check if p1 is directly before or after p2
		return vertices[ip1].equals(p1) || vertices[ip3].equals(p1);
	}
	
	@Override
	public double sdf(Vector2D p) {
		double d = p.subtract(vertices[0]).dotProduct(p.subtract(vertices[0]));
		double s = 1.0;
		for (int i = 0, j = vertices.length - 1; i < vertices.length; j = i, i++) {
			Vector2D e = vertices[j].subtract(vertices[i]);
			Vector2D w = p.subtract(vertices[i]);
			double dot_we = w.dotProduct(e);
			double dot_ee = e.dotProduct(e);
			Vector2D b = w.subtract(e.scalarMultiply(clamp(dot_we / dot_ee, 0.0, 1.0)));
			d = Math.min(d, b.dotProduct(b));

			boolean[] c = {
					p.getY() >= vertices[i].getY(),
					p.getY() < vertices[j].getY(),
					e.getX() * w.getY() > e.getY() * w.getX()
					};
			
			if (all(c) || allNot(c)) s *= -1.0;
		}
		return s * Math.sqrt(d);
	}
	
	@Override
	public double height(Vector2D p) {
		return h;
	}
	
	@Override
	public void paint(GraphicsContext g) {
		for (int i = 1; i < vertices.length; i++) {
			g.strokeLine(vertices[i-1].getX(), vertices[i-1].getY(), vertices[i].getX(), vertices[i].getY());
		}
		g.strokeLine(vertices[vertices.length-1].getX(), vertices[vertices.length-1].getY(), vertices[0].getX(), vertices[0].getY());
	}
	
	private final double clamp(double a, double min, double max) {
		return Math.min(Math.max(a, min), max);
	}
	
	private final boolean all(boolean[] bs) {
		for (boolean b: bs) {
			if (!b) return false;
		}
		return true;
	}
	
	private final boolean allNot(boolean[] bs) {
		for (boolean b: bs) {
			if (b) return false;
		}
		return true;
	}

	@Override
	public void collectPoints(Map<Vector2D, Double> pointsCollection) {
		for (Vector2D p: vertices) {
			pointsCollection.put(p, h);
		}
	}

	@Override
	public void collect2dVerticies(Collection<Vector2D> verticies) {
		for (Vector2D v : vertices) {
			verticies.add(v);
		}
	}

	@Override
	public void collect3dVerticies(Collection<Vector3D> verticies) {
		for (Vector2D v : vertices) {
			verticies.add(new Vector3D(v.getX(), v.getY(), h));
		}
	}
}
