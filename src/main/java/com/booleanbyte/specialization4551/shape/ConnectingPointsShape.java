package com.booleanbyte.specialization4551.shape;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

public interface ConnectingPointsShape {

	/**
	 * Check if two points are connected by this shape, if they they are both
	 * present in the shape.
	 * <p>
	 * The invoker of the method needs not guarantee that any of the points need be
	 * present in the shape.
	 * 
	 * @param p1
	 * @param p2
	 * @return {@code true} if p1 and p2 are connected, false otherwise
	 */
	public boolean areConnected(Vector2D p1, Vector2D p2);
}
