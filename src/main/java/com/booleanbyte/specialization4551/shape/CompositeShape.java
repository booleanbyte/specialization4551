package com.booleanbyte.specialization4551.shape;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import javafx.scene.canvas.GraphicsContext;

public class CompositeShape extends AbstractShape implements ConnectingPointsShape {
	
	private final List<? extends AbstractShape> shapes;
	
	public CompositeShape(List<? extends AbstractShape> shapes) {
		this.shapes = shapes;
	}
	
	@Override
	public boolean areConnected(Vector2D p1, Vector2D p2) {
		for (AbstractShape shape: shapes) {
			if (shape instanceof ConnectingPointsShape) {
				if (((ConnectingPointsShape) shape).areConnected(p1, p2)) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Calculated the minimum distance to any shape in the composite at point p
	 */
	@Override
	public double sdf(Vector2D p) {
		double minDist = Double.POSITIVE_INFINITY;
		for (AbstractShape shape: shapes) {
			minDist = Math.min(minDist, shape.sdf(p));
		}
		return minDist;
	}
	
	/**
	 * Getting the height of a composite shape is unsupported
	 */
	@Override
	public double height(Vector2D p) {
		throw new UnsupportedOperationException("Cannot get a single height for a composite shape");
	}
	
	/**
	 * Adds the weights and heights from all component shapes in the composite to the sum
	 * at point p
	 */
	@Override
	public void sumWH(Vector2D p, WeigthedAverage sumWH, Function<Double, Double> distanceWeightFunction) {
		for (AbstractShape shape: shapes) {
			shape.sumWH(p, sumWH, distanceWeightFunction);
		}
	}

	@Override
	public void paint(GraphicsContext g) {
		for (AbstractShape shape: shapes) {
			shape.paint(g);
		}
	}

	@Override
	public void collectPoints(Map<Vector2D, Double> pointsCollection) {
		for (AbstractShape shape: shapes) {
			shape.collectPoints(pointsCollection);
		}
	}

	@Override
	public void collect2dVerticies(Collection<Vector2D> verticies) {
		for (AbstractShape s : shapes) {
			s.collect2dVerticies(verticies);
		}
	}

	@Override
	public void collect3dVerticies(Collection<Vector3D> verticies) {
		for (AbstractShape s : shapes) {
			s.collect3dVerticies(verticies);
		}
	}
}
