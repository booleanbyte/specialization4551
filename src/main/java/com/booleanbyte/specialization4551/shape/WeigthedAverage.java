package com.booleanbyte.specialization4551.shape;

public class WeigthedAverage {
	
	private double weightSum = 0.0;
	private double weightedHeightSum = 0.0;
	
	public void addWeightAndHeight(double weight, double height) {
		weightSum += weight;
		weightedHeightSum += height * weight;
	}
	
	public double getWeightedAverage() {
		return weightedHeightSum / weightSum;
	}
}
