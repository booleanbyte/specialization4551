package com.booleanbyte.specialization4551.shape;

public class Shapes {
	
	public static boolean lineLineConflict(Line line1, Line line2) {
		// TODO Implement line-line intersection detection
		return false;
	}
	
	public static boolean polygonLineConflict(Polygon polygon, Line line) {
		if (polygon.sdf(line.c1) <= 0) return true;
		if (polygon.sdf(line.c2) <= 0) return true;
		// TODO Implement line intersections for polygon edges
		return false;
	}
}
