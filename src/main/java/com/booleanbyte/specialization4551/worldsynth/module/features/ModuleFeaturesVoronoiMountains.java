package com.booleanbyte.specialization4551.worldsynth.module.features;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import com.booleanbyte.specialization4551.shape.AbstractShape;
import com.booleanbyte.specialization4551.shape.ConnectingPointsShape;
import com.booleanbyte.specialization4551.shape.Line;
import com.booleanbyte.specialization4551.worldsynth.datatype.DatatypeFeatures;

import de.alsclo.voronoi.Voronoi;
import de.alsclo.voronoi.graph.Edge;
import de.alsclo.voronoi.graph.Graph;
import de.alsclo.voronoi.graph.Vertex;
import javafx.scene.paint.Color;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.FloatParameter;

public class ModuleFeaturesVoronoiMountains extends AbstractModule {
	
	private final FloatParameter elevation = new FloatParameter("ridgeelevation", "Ridge elevation",
			"Ridge height added relative to highest near feature",
			50.0f, 0.0f, Float.POSITIVE_INFINITY, 0.0f, 200.0f);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				elevation
				};
		return p;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("features", new ModuleInputRequest(getInput("Features"), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeFeatures requestData = (DatatypeFeatures) request.data;
		
		//----------READ INPUTS----------//
		
		float elevation = this.elevation.getValue();
		
		//Check if both inputs are available
		if (inputs.get("features") == null) {
			//If the input is null, there is not enough input and then just return null
			return null;
		}
		//Read in the primary and secondary
		List<AbstractShape> inputfeatures = ((DatatypeFeatures) inputs.get("features")).getFeatures();
		
		//----------BUILD----------//
		
		// Get all shape points in features
		HashMap<Vector2D, Double> shapePoints = new HashMap<>();
		for (AbstractShape shape: inputfeatures) {
			shape.collectPoints(shapePoints);
		}
		
		// Convert shape points to voronoi points
		HashMap<de.alsclo.voronoi.graph.Point, Vector2D> voronoiPoints = new HashMap<>();
		for (Vector2D p: shapePoints.keySet()) {
			voronoiPoints.put(new de.alsclo.voronoi.graph.Point(p.getX(), p.getY()), p);
		}
		
		Voronoi voronoi = new Voronoi(voronoiPoints.keySet());
		Graph voronoiGraph = voronoi.getGraph();
		
		
		
		// Add edges as shapes
		ArrayList<Line> edgeLines = new ArrayList<>();
		
		ArrayList<Edge> separatingEdges = new ArrayList<>();
		HashMap<Vertex, Double> vertexHeight = new HashMap<>();
		
		Stream<Edge> edgeStream = voronoiGraph.edgeStream();
		Iterator<Edge> it = edgeStream.iterator();
		while (it.hasNext()) {
			Edge e = it.next();
			
			boolean connected = false;
			for (AbstractShape shape: inputfeatures) {
				if (shape instanceof ConnectingPointsShape) {
					Vector2D site1 = voronoiPoints.get(e.getSite1());
					Vector2D site2 = voronoiPoints.get(e.getSite2());
					double siteHeightMax = Math.max(shapePoints.get(site1), shapePoints.get(site2));
					
					// Store maximum edge vertex height based on bordering sites
					Double heightA = vertexHeight.get(e.getA());
					vertexHeight.put(e.getA(), heightA == null ? siteHeightMax : Math.max(heightA, siteHeightMax));
					Double heightB = vertexHeight.get(e.getB());
					vertexHeight.put(e.getB(), heightB == null ? siteHeightMax : Math.max(heightB, siteHeightMax));
					
					connected = ((ConnectingPointsShape) shape).areConnected(site1, site2) ? true : connected;
				}
			}
			if (!connected) {
				separatingEdges.add(e);
			}
		}
		
		for (Edge e: separatingEdges) {
			if (e.getA() != null && e.getB() != null) {
				Vector2D p1 = new Vector2D(e.getA().getLocation().x, e.getA().getLocation().y);
				Vector2D p2 = new Vector2D(e.getB().getLocation().x, e.getB().getLocation().y);
				
				Line ridgeSegment = new Line(p1, elevation+vertexHeight.get(e.getA()), p2, elevation+vertexHeight.get(e.getB()));
				ridgeSegment.setAnnotationColor(Color.WHITE);
				edgeLines.add(ridgeSegment);
			}
		}
		
		ArrayList<AbstractShape> ridgeShapes = new ArrayList<AbstractShape>();
		ridgeShapes.addAll(edgeLines);
		requestData.setFeatures(ridgeShapes);
		
		return requestData;
	}

	@Override
	public String getModuleName() {
		return "Voronoi mountains";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeFeatures(), "Features")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeFeatures(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
}
