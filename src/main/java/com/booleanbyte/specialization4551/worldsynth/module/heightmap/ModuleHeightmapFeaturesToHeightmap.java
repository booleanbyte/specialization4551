package com.booleanbyte.specialization4551.worldsynth.module.heightmap;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import com.booleanbyte.specialization4551.shape.AbstractShape;
import com.booleanbyte.specialization4551.shape.WeigthedAverage;
import com.booleanbyte.specialization4551.worldsynth.datatype.DatatypeFeatures;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;
import net.worldsynth.parameter.EnumParameter;

public class ModuleHeightmapFeaturesToHeightmap extends AbstractModule {
	
	private EnumParameter<WeightFunction> weightFunction = new EnumParameter<WeightFunction>(
			"weightfunction", "Weight function",
			"The distance weight function to use  evaluation the weigted average",
			WeightFunction.class, WeightFunction.W3);
	
	private DoubleParameter aParam = new DoubleParameter(
			"a", "a",
			"The a variable used in weight function W2 and W3",
			250.0, 0.0, Double.POSITIVE_INFINITY, 100, 500);
	
	private DoubleParameter bParam = new DoubleParameter(
			"b", "b",
			"The b variable used in weight function W3",
			1.0, 0.0, Double.POSITIVE_INFINITY, 0.0, 2.0);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				weightFunction,
				aParam,
				bParam
		};
		return p;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("features", new ModuleInputRequest(getInput("Features"), new DatatypeFeatures()));
		
		return inputRequests;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		double x = requestData.extent.getX();
		double z = requestData.extent.getZ();
		double res = requestData.resolution;
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		WeightFunction w = this.weightFunction.getValue();
		double a = this.aParam.getValue();
		double b = this.bParam.getValue();
		
		//Check if inputs are available
		if (inputs.get("features") == null) {
			//If there is not enough input just return null
			return null;
		}
				
		DatatypeFeatures featuresData = (DatatypeFeatures) inputs.get("features");
		
		//----------BUILD----------//
		
		float[][] map = new float[mpw][mpl];
		
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				float h = (float) getHeightAt(x+u*res, z+v*res, featuresData.getFeatures(), w, a, b);
				map[u][v] = h;
			}
		}
		
		requestData.setHeightmap(map);
		
		return requestData;
	}
	
	public double getHeightAt(double x, double z, List<AbstractShape> features, WeightFunction w, double a, double b) {
		Vector2D p = new Vector2D(x, z);
		
		// The distance to weight function for blending the heights.
		Function<Double, Double> distanceWeightFunction = d -> d; // Default no weight function
		switch (w) {
		case W1:
			distanceWeightFunction = d -> {
				d = Math.max(0.0, d);
				return 1/d;
				};
			break;
		case W2:
			distanceWeightFunction = d -> {
				d = Math.max(0.0, d);
				double da = d/a;
//				double db = d/b;
				return Math.max(0, (1-da)/d);
//				return Math.max(0, (1-da)/db);
				};
			break;
		case W3:
			distanceWeightFunction = d -> {
				d = Math.max(0.0, d);
				double da = d/a;
				double db = d/b;
				return Math.max(0, (1-da*da*da*da)/(db*db+1));
				};
			break;
		}
		
		WeigthedAverage avg = new WeigthedAverage();
		
		for (AbstractShape s: features) {
			s.sumWH(p, avg, distanceWeightFunction);
		}
		
		double height = avg.getWeightedAverage();
		return height;
	}

	@Override
	public String getModuleName() {
		return "Features to heightmap";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeFeatures(), "Features")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	private enum WeightFunction {
		W1, W2, W3;
	}
}
