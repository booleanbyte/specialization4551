package com.booleanbyte.specialization4551.worldsynth.module.features;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import com.booleanbyte.specialization4551.shape.AbstractShape;
import com.booleanbyte.specialization4551.shape.Line;
import com.booleanbyte.specialization4551.shape.Polygon;
import com.booleanbyte.specialization4551.worldsynth.datatype.DatatypeFeatures;

import javafx.scene.paint.Color;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;
import net.worldsynth.parameter.IntegerParameter;

public class ModuleFeaturesLakeAndRiverNetworkGenerator extends AbstractModule {

	//----------RIVER PARAMETERS----------//
	private final DoubleParameter segementLenght = new DoubleParameter("minimumsegmentlenght", "Minimum segment length",
			"Minimum lenght of a single river segment",
			50, 1, Double.POSITIVE_INFINITY, 1, 200);
	
	private final DoubleParameter segmentLenghtFactor = new DoubleParameter("maximumsegmentlenghtfactor", "Maximum segment length factor",
			"Multiplication factor for random additional max segement length",
			0.5, 0.0, 1.0);
	
	private final DoubleParameter slope = new DoubleParameter("slope", "Slope",
			"How steep the river rises away from the shore",
			0.1, 0.0, 1.0);
	
	//----------LAKE PARAMETERS----------//
	private final DoubleParameter size = new DoubleParameter("size", "Lake size",
			"Aproximate radius of a lake",
			100, 0, Double.POSITIVE_INFINITY, 0, 1000);
	
	private final IntegerParameter initialVertices = new IntegerParameter("initialvertices", "Lake initial vertices",
			"The number of verticies in the initial step of a lake shape",
			6, 3, Integer.MAX_VALUE, 3, 18);
	
	private final DoubleParameter sizeVariation = new DoubleParameter("sizevariation", "Lake size variation",
			"The max deviation of the initial shape from the size radius",
			100, 0, Double.POSITIVE_INFINITY, 0, 200);
	
	private final IntegerParameter midpointDisplacements = new IntegerParameter("midpointdisplacements", "Lake midpoint displacements",
			"The number of midpoint displacement iteratoions to perform",
			1, 0, 10);
	
	private final DoubleParameter midpointDisplacementFactor = new DoubleParameter("midpointdisplacementfactor", "Lake midpoint displacement factor",
			"The relative max amount of midpoint displacement to perform",
			0.4, 0.0, 0.5);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				segementLenght,
				segmentLenghtFactor,
				slope,
				size,
				initialVertices,
				sizeVariation,
				midpointDisplacements,
				midpointDisplacementFactor
				};
		return p;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("landmass", new ModuleInputRequest(getInput("Landmass"), new DatatypeFeatures()));
		
		return inputRequests;
	}

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeFeatures requestData = (DatatypeFeatures) request.data;
		
		//----------READ INPUTS----------//
		
		double segementLenght = this.segementLenght.getValue();
		double segmentLenghtFactor = this.segmentLenghtFactor.getValue();
		double slopeAngle = Math.atan(this.slope.getValue());
		
		double size = this.size.getValue();
		int initialVertices = this.initialVertices.getValue();
		double sizeVariation = this.sizeVariation.getValue();
		int midpointDisplacements = this.midpointDisplacements.getValue();
		double midpointDisplacementFactor = this.midpointDisplacementFactor.getValue();
		
		//Check if inputs are available
		if (inputs.get("landmass") == null) {
			//If there is not enough input just return null
			return null;
		}
				
		DatatypeFeatures landmassData = (DatatypeFeatures) inputs.get("landmass");
		List<AbstractShape> landmassShapes = (List<AbstractShape>) landmassData.getFeatures();
		
		//----------BUILD----------//
		
		ArrayList<RiverNode> riverNodes = new ArrayList<>();
		ArrayList<AbstractShape> lakesShapes = new ArrayList<>();
		
		// Get all landmass verticies
		HashSet<Vector3D> landmassVerticies = new HashSet<Vector3D>();
		for (AbstractShape s : landmassShapes) {
			s.collect3dVerticies(landmassVerticies);
		}
		
		// Make seeding nodes
		LinkedList<RiverNode> seedingNodes = new LinkedList<>();
		for (Vector3D v: landmassVerticies) {
			if (validatePlacement(v, riverNodes, null, lakesShapes, segementLenght, null)) {
				RiverNode n = new RiverNode(v, null);
				riverNodes.add(n);
				seedingNodes.add(n);
			}
		}
		
		Random r = new Random(0);
		
		int maxSamples = 30;
		while (seedingNodes.size() > 0) {
			int inx = r.nextInt(seedingNodes.size());
			RiverNode seedingNode = seedingNodes.remove(inx);
			
			for (int i = 0; i < maxSamples; i++) {
				// Decide a random length and direction towards where to try a new node at
				double rdist = segementLenght + segementLenght * segmentLenghtFactor * r.nextDouble();
				double rdir = Math.PI * (r.nextDouble() * 2.0 - 1.0);
				
				
				if (r.nextDouble() < 0.2) {
					// Create a new lake
					double elevation = seedingNode.vert.getZ();
					LinkedList<Vector3D> lakeVertices = createLake(seedingNode.vert, rdir, elevation, r, size, initialVertices, sizeVariation, midpointDisplacements, midpointDisplacementFactor);
					
					// Check if the lake is a valid placement for a new lake feature
					if (validateLakePlacement(lakeVertices, riverNodes, seedingNode, lakesShapes, segementLenght, landmassShapes)) {
						// Create seeding nodes for lake
						for (Vector3D v: lakeVertices) {
							if (validatePlacement(v, riverNodes, null, lakesShapes, segementLenght, null)) {
								RiverNode n = new RiverNode(v, null);
								riverNodes.add(n);
								seedingNodes.add(n);
							}
						}
						
						lakesShapes.add(createLakeShape(lakeVertices, elevation));
						break;
					}
				}
				else {
					// Create a new vertex vector
					Vector3D v = new Vector3D(rdir, slopeAngle).scalarMultiply(rdist).add(seedingNode.vert);
					
					// Check if the new vertex is a valid placement for a new river node
					if (validatePlacement(v, riverNodes, null, lakesShapes, segementLenght, landmassShapes)) {
						RiverNode n = new RiverNode(v, seedingNode);
						riverNodes.add(n);
						
						if (r.nextDouble() < 0.5) {
							seedingNodes.addFirst(n);
						}
						else {
							seedingNodes.add(n);
						}
						seedingNodes.add(seedingNode);
						
						break;
					}
				}
			}
		}
		
		// Generated features
		ArrayList<AbstractShape> waterSystem = new ArrayList<>();
		waterSystem.addAll(lakesShapes);
		waterSystem.addAll(createRiverNetworkShapes(riverNodes));
		
		requestData.setFeatures(waterSystem);
		return requestData;
	}
	
	private boolean validatePlacement(Vector3D v, List<RiverNode> nodes, RiverNode exept, List<AbstractShape> lakes, double minDist, List<AbstractShape> landmassShapes) {
		Vector2D v2d = new Vector2D(v.getX(), v.getY());
		
		double d = Double.POSITIVE_INFINITY;
		for (RiverNode n: nodes) {
			if (n == exept) continue;
			Vector2D vn = new Vector2D(n.vert.getX(), n.vert.getY());
			d = Math.min(d, vn.distance(v2d));
		}
		
		for (AbstractShape l: lakes) {
			d = Math.min(d, l.sdf(v2d));
		}
		
		if (landmassShapes != null) {
			for (AbstractShape s : landmassShapes) {
				if (s.sdf(v2d) < minDist) return false;
			}
		}
		return d > minDist;
	}
	
	public List<AbstractShape> createRiverNetworkShapes(List<RiverNode> riverNodes) {
		ArrayList<AbstractShape> riverSegments = new ArrayList<>();
		
		for (RiverNode node : riverNodes) {
			if (node.parent == null) continue;
			
			Line riverSegment = new Line(
					node.parent.vert.getX(),
					node.parent.vert.getY(),
					node.parent.vert.getZ(),
					node.vert.getX(),
					node.vert.getY(),
					node.vert.getZ());
			riverSegment.setAnnotationColor(Color.CADETBLUE);
			riverSegments.add(riverSegment);
		}
		
		return riverSegments;
	}
	
	private LinkedList<Vector3D> createLake(Vector3D seedVertex, double lakeDirection, double elevation, Random r, double size, int initialVertices, double sizeVariation, int midpointDisplacements, double midpointDisplacementFactor) {
		int vs = initialVertices;
		int subs = midpointDisplacements;
		
		LinkedList<Vector3D> vertexes = new LinkedList<>();
		
		vertexes.add(seedVertex);
		Vector3D centerVertex = seedVertex.add(new Vector3D(lakeDirection, 0.0).scalarMultiply(size + (r.nextDouble() - 0.5) * sizeVariation));
		for (int i = 1; i < vs; i++) {
			double mod = (r.nextDouble() - 0.5) * sizeVariation;
			Vector3D v = new Vector3D(lakeDirection + Math.PI + i * (2 * Math.PI / (double) vs), 0.0).scalarMultiply(size + mod).add(centerVertex);
			vertexes.add(v);
		}
		
		// Midpoint displacement
		for (int i = 1; i <= subs; i++) {
			int j = 0;
			while (j < vertexes.size()) {
				Vector3D v1 = vertexes.get(j);
				Vector3D v2 = vertexes.get((j+1)%vertexes.size());
				Vector3D dir = v2.subtract(v1);
				Vector3D norm = new Vector3D(-dir.getY(), dir.getX(), 0.0).normalize();
				
				double displacement = (r.nextDouble()*2.0-1.0)*dir.getNorm()*midpointDisplacementFactor;
				Vector3D v15 = v1.add(dir.scalarMultiply(0.5)).add(norm.scalarMultiply(displacement));
				
				vertexes.add(++j, v15);
				j++;
			}
		}
		
		return vertexes;
	}
	
	private boolean validateLakePlacement(LinkedList<Vector3D> lakeVerts, List<RiverNode> nodes, RiverNode except, List<AbstractShape> lakeShapes, double minDist, List<AbstractShape> landmassShapes) {
		for (Vector3D v: lakeVerts) {
			if (!validatePlacement(v, nodes, except, lakeShapes, minDist, landmassShapes)) return false;
		}
		return true;
	}
	
	public AbstractShape createLakeShape(List<Vector3D> lakeVertices, double elevation) {
		Vector2D[] landmassShapeVertices = new Vector2D[lakeVertices.size()];
		for (int i = 0; i < lakeVertices.size(); i++) {
			landmassShapeVertices[i] = new Vector2D(lakeVertices.get(i).getX(), lakeVertices.get(i).getY());
		}
		
		Polygon lake = new Polygon(elevation, landmassShapeVertices) {
			@Override
			public double height(Vector2D p) {
				return super.height(p);
			}
		};
		lake.setAnnotationColor(Color.ORANGE);
		return lake;
	}

	@Override
	public String getModuleName() {
		return "Lake and River network generator";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeFeatures(), "Landmass")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeFeatures(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	private class RiverNode {
		public final Vector3D vert;
		public final RiverNode parent;
		
		public RiverNode(Vector3D vert, RiverNode parent) {
			this.vert = vert;
			this.parent = parent;
		}
	}
}
