package com.booleanbyte.specialization4551.worldsynth.module;

import com.booleanbyte.specialization4551.worldsynth.module.features.ModuleFeatureConflictRemoval;
import com.booleanbyte.specialization4551.worldsynth.module.features.ModuleFeaturesCollect;
import com.booleanbyte.specialization4551.worldsynth.module.features.ModuleFeaturesLakeAndRiverNetworkGenerator;
import com.booleanbyte.specialization4551.worldsynth.module.features.ModuleFeaturesLandmassGenerator;
import com.booleanbyte.specialization4551.worldsynth.module.features.ModuleFeaturesRiverNetworkGenerator;
import com.booleanbyte.specialization4551.worldsynth.module.features.ModuleFeaturesVoronoiMountains;
import com.booleanbyte.specialization4551.worldsynth.module.heightmap.ModuleHeightmapFeaturesToHeightmap;
import com.booleanbyte.specialization4551.worldsynth.module.heightmap.ModuleHeightmapFeaturesToSdfHeightmap;

import net.worldsynth.module.AbstractModuleRegister;
import net.worldsynth.module.ClassNotModuleExeption;

public class SpecializationModuleRegister extends AbstractModuleRegister {
	
	public SpecializationModuleRegister() {
		super();
		
		try {
			registerModule(ModuleFeaturesLandmassGenerator.class, "\\Specialization");
			registerModule(ModuleFeaturesRiverNetworkGenerator.class, "\\Specialization");
			registerModule(ModuleFeaturesLakeAndRiverNetworkGenerator.class, "\\Specialization");
			registerModule(ModuleFeaturesVoronoiMountains.class, "\\Specialization");
			registerModule(ModuleFeaturesCollect.class, "\\Specialization");
			registerModule(ModuleFeatureConflictRemoval.class, "\\Specialization");
			
			registerModule(ModuleHeightmapFeaturesToHeightmap.class, "\\Specialization");
			registerModule(ModuleHeightmapFeaturesToSdfHeightmap.class, "\\Specialization");
			
		} catch (ClassNotModuleExeption e) {
			throw new RuntimeException(e);
		}
	}
}
