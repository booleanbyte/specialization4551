package com.booleanbyte.specialization4551.worldsynth.datatype;

import java.util.ArrayList;
import java.util.List;

import com.booleanbyte.specialization4551.shape.AbstractShape;
import com.booleanbyte.specialization4551.worldsynth.patcher.ui.preview.FeaturesSdfRender;
import com.booleanbyte.specialization4551.worldsynth.patcher.ui.preview.FeaturesSimplifiedRender;

import javafx.scene.paint.Color;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRender;

public class DatatypeFeatures extends AbstractDatatype {
	
	private List<AbstractShape> features;
	
	public List<AbstractShape> getFeatures() {
		return features;
	}
	
	public void setFeatures(List<AbstractShape> features) {
		this.features = features;
	}
	
	@Override
	public Color getDatatypeColor() {
		return Color.BLUE;
	}

	@Override
	public String getDatatypeName() {
		return "Features";
	}

	@Override
	public AbstractDatatype clone() {
		DatatypeFeatures df = new DatatypeFeatures();
		if (features != null) df.features = new ArrayList<AbstractShape>(features);
		return df;
	}

	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		return new DatatypeFeatures();
	}
	
	@Override
	public AbstractPreviewRender getPreviewRender() {
//		return new FeaturesSdfRender();
		return new FeaturesSimplifiedRender();
	}
}
