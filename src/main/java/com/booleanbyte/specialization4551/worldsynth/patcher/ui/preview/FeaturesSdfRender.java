package com.booleanbyte.specialization4551.worldsynth.patcher.ui.preview;

import java.util.List;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import com.booleanbyte.specialization4551.shape.AbstractShape;
import com.booleanbyte.specialization4551.worldsynth.datatype.DatatypeFeatures;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRenderCanvas;
import net.worldsynth.synth.SynthParameters;

public class FeaturesSdfRender extends AbstractPreviewRenderCanvas {
	
	private List<AbstractShape> features;
	
	@Override
	public void pushDataToRender(AbstractDatatype data, SynthParameters synthParameters) {
		DatatypeFeatures castData = (DatatypeFeatures) data;
		this.features = castData.getFeatures();
		paint();
	}
	
	@Override
	public void paint() {
		GraphicsContext g = getGraphicsContext2D();
		g.setFill(Color.gray(0.0));
		g.fillRect(0, 0, getWidth(), getHeight());
		
		g.save();
		g.translate(getWidth()/2, getHeight()/2);
		for (int x = -(int)getWidth()/2; x < (int)getWidth()/2; x++) {
			for (int z = -(int)getWidth()/2; z < (int)getWidth()/2; z++) {
				double minDist = Double.POSITIVE_INFINITY;
				for (AbstractShape f: features) {
					minDist = Math.min(minDist, f.sdf(new Vector2D(x, z)));
				}
				
				g.setFill(distanceToColor(minDist));
				g.fillRect(x, z, 1, 1);
			}
		}
		g.restore();
	}
	
	private Color distanceToColor(double distance) {
		if (distance < 0) {
			distance = Math.sin(-distance / 2.5)/2;
			return Color.color(distance < 0 ? 0.8 : 1, 0, 0);
		}
		distance = Math.sin(distance / 2.5)/2;
		return Color.color(0, distance < 0 ? 0.8 : 1, 0);
	}
}
